[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/A70m0s/margarine/-/blob/master/LICENSE)


# Margarine

> Full featured OS for your Numworks Calculator

![Margarine](themes/Omega.png)

Margarine is a fork of [@Omega](https://github.com/Omega-Numworks/Omega), an open source OS for Numworks Calculators.

## Fun facts :

- The name of this project comes from a link in my mind between Omega and Saint-Hubert Omega 3, a french butter brand. So I decided to name this fork "Margarine", with a butter brand's logo type :wink:
- It was originally developped to allow keeping python scripts (only snake :innocent:) when exam mode is activated. But the lockdown has given to me some ideas...

## Warning !

This project is in developpement so it's not fully usable now.

:no_entry: Please don't try it at home ! :no_entry: